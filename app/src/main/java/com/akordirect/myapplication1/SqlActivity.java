package com.akordirect.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.akordirect.myapplication1.domain.ContactMy;

import java.util.ArrayList;
import java.util.List;

public class SqlActivity extends AppCompatActivity {

//    private TextView textView;
    private static final String TAG = "SqlActivity";
    private ListView contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql);

//        textView = findViewById(R.id.textView_inf);

        contacts = findViewById(R.id.contact_names);

        SQLiteDatabase sqLiteDatabase = getBaseContext().openOrCreateDatabase("test.db", MODE_PRIVATE, null);
        String sql = "DROP TABLE IF EXISTS contacts";
        sqLiteDatabase.execSQL(sql);
        sql = "CREATE TABLE IF NOT EXISTS contacts (name TEXT, phone INTEGER, email TEXT)";
        sqLiteDatabase.execSQL(sql);
        sql = "INSERT INTO contacts VALUES('tim', 4444, '555@gmail.com')";
        sqLiteDatabase.execSQL(sql);
        sql = "INSERT INTO contacts VALUES('Dmitry', 2558888, '2558888@gmail.com')";
        sqLiteDatabase.execSQL(sql);

        Cursor query = sqLiteDatabase.rawQuery("SELECT * FROM contacts;", null);

//        List<ContactMy> contactMyList = new ArrayList<>();
        List<String> names = new ArrayList<>();

        int x=1;
        if(query.moveToFirst()){
            do {
                String name = query.getString(0);
                int phone = query.getInt(1);
                String email = query.getString(2);
                Log.d("SqlActivity", "name = " + name);
//                Toast.makeText(this,"name = " + name , Toast.LENGTH_LONG);
//                textView.setText("name = " + name);
                names.add(x++ + ") " + name + " ("+email+")");
//                contactMyList.add(new ContactMy(name, email));

            }while (query.moveToNext());
        }

        query.close();
        sqLiteDatabase.close();

        ArrayAdapter<String> adapter =  new ArrayAdapter<>(
                SqlActivity.this,
                        R.layout.contact_detail,
                        R.id.text_contact_name,
                        names);
//        ArrayAdapter<String> adapter_email =  new ArrayAdapter<>(
//                SqlActivity.this,
//                        R.layout.contact_detail,
//                        R.id.text_contact_email,
//                        names);
        contacts.setAdapter(adapter);
    }
}
