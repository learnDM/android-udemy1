package com.akordirect.myapplication1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SwitchActivity extends AppCompatActivity {

    private String TAG = "SwitchActivity";

    private TextView textView;
    private Button button1;

    private int numTimeOfClicked = 0;
    private final String TEXT_CONTAINS = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch);

        button1  = findViewById(R.id.button_click);
        textView = findViewById(R.id.textView);

        textView.setText("");
        textView.setMovementMethod(new ScrollingMovementMethod());

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numTimeOfClicked = numTimeOfClicked + 1;
                String result  = "\nClicked " + numTimeOfClicked + " time";
                if(numTimeOfClicked!=0) result += "s";

                textView.append(result);
            }
        };

        if(button1 != null){
            button1.setOnClickListener(onClickListener);
        }

        Log.e(TAG, "onCreate: off");
    }


    @Override
    protected void onStart() {
        Log.e(TAG, "onStart: in");
        super.onStart();
        Log.e(TAG, "onStart: off");
    }


    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        Log.e(TAG, "onRestoreInstanceState: in");

        super.onRestoreInstanceState(savedInstanceState);
        textView.setText(savedInstanceState.getString(TEXT_CONTAINS));
        Log.e(TAG, "onRestoreInstanceState: off");
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: in");
        super.onResume();
        Log.e(TAG, "onResume: off");
    }




    @Override
    protected void onPause() {
        Log.e(TAG,"onPause: in");
        super.onPause();
        Log.e(TAG,"onPause: off");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.e(TAG, "onSaveInstanceState: in");
        outState.putString(TEXT_CONTAINS, textView.getText().toString());
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState: off");
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "onStop: in");
        super.onStop();
        Log.e(TAG, "onStop: in");
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy: in");
        super.onDestroy();
        Log.e(TAG, "onDestroy: off");
    }










}
