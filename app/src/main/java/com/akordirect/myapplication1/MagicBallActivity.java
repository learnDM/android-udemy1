package com.akordirect.myapplication1;

//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MagicBallActivity extends AppCompatActivity {

    private static final String TAG = "MagicBallActivity";

    private ImageView ballDisplay;
    private Button myButton;
    private final int[] ballArray = new int[] {
            R.drawable.ball1,
            R.drawable.ball2,
            R.drawable.ball3,
            R.drawable.ball4,
            R.drawable.ball5,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magic_ball);

        ballDisplay = findViewById(R.id.image_ball);
        myButton = findViewById(R.id.button_ball_ask);

        myButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Random randomNumberGenerator = new Random();
                int number = randomNumberGenerator.nextInt(4);
                Log.d(TAG, "Random number = " + number);
                ballDisplay.setImageResource(ballArray[number]);
            }
        });

    }


}
