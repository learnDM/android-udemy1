package com.akordirect.myapplication1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private Button buttonCalculator;
    private Button buttonDownload;
    private Button buttonDice;
    private Button buttonMusic;
    private Button buttonQuizzer;
    private Button buttonSwitch;
    private Button buttonBall;
    private Button buttonWeather;
    private Button buttonSql;

    private String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCalculator = findViewById(R.id.buttonCalculator);
        buttonDownload = findViewById(R.id.buttonDownload);
        buttonDice = findViewById(R.id.buttonDice);
        buttonMusic = findViewById(R.id.buttonMusic);
        buttonQuizzer = findViewById(R.id.buttonQuezzer);
        buttonSwitch = findViewById(R.id.button_switch);
        buttonBall = findViewById(R.id.button_go_to_magic_ball);
        buttonWeather = findViewById(R.id.button_go_to_weather);
        buttonSql = findViewById(R.id.button_go_to_sql);

        if (buttonCalculator != null) {
            buttonCalculator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        Intent k = new Intent(MainActivity.this, CalculatorActivity.class);
                        startActivity(k);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });
        }

        if (buttonDownload != null) {
            buttonDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent k = new Intent(MainActivity.this, DownloadActivity.class);
                        startActivity(k);
                    } catch (Exception e) {
                        Log.d(TAG, "dm-> Problem DownloadActivity");
                        e.printStackTrace();
                    }
                }
            });

        }

        if (buttonDice != null) {
            buttonDice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent goToDice = new Intent(MainActivity.this, DiceActivity.class);
                        startActivity(goToDice);
                    } catch (Exception e) {
                        Log.d(TAG, "dm-> Problem DiceActivity");
                        e.printStackTrace();
                    }

                }
            });
        }

        if (buttonMusic != null) {
            buttonMusic.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent goToMusic = new Intent(MainActivity.this, MusicActivity.class);
                    startActivity(goToMusic);
                }
            });
        }

        if (buttonQuizzer != null) {
            buttonQuizzer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, QuizzlerActivity.class));
                }
            });
        }


        if (buttonSwitch != null) {
            buttonSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, SwitchActivity.class));
                }
            });
        }


        if (buttonBall != null) {
            buttonBall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, MagicBallActivity.class));
                }
            });
        }
        if (buttonWeather != null) {
            buttonWeather.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, WeatherController.class));
                }
            });
        }

        if (buttonSql != null) {
            buttonSql.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, SqlActivity.class));
                }
            });
        }


        Log.e(TAG, "onCreate: off");
    }


}
